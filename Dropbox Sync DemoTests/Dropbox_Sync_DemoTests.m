//
//  Dropbox_Sync_DemoTests.m
//  Dropbox Sync DemoTests
//
//  Created by Matthew Lewis on 6/11/14.
//  Copyright (c) 2014 Kestrel Development. All rights reserved.
//

#import <XCTest/XCTest.h>

@interface Dropbox_Sync_DemoTests : XCTestCase

@end

@implementation Dropbox_Sync_DemoTests

- (void)setUp
{
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testExample
{
    XCTFail(@"No implementation for \"%s\"", __PRETTY_FUNCTION__);
}

@end
