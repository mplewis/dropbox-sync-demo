//
//  DBSDDismissModal.m
//  Dropbox Sync Demo
//
//  Created by Matthew Lewis on 6/11/14.
//  Copyright (c) 2014 Kestrel Development. All rights reserved.
//

#import "DBSDDismissModal.h"

@implementation DBSDDismissModal

- (void)perform
{
    UIViewController *sourceViewController = self.sourceViewController;
    [sourceViewController.presentingViewController dismissViewControllerAnimated:YES completion:nil];
}

@end
