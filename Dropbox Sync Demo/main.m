//
//  main.m
//  Dropbox Sync Demo
//
//  Created by Matthew Lewis on 6/11/14.
//  Copyright (c) 2014 Kestrel Development. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "DBSDAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([DBSDAppDelegate class]));
    }
}
