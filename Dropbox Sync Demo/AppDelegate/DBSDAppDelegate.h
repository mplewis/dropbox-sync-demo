//
//  DBSDAppDelegate.h
//  Dropbox Sync Demo
//
//  Created by Matthew Lewis on 6/11/14.
//  Copyright (c) 2014 Kestrel Development. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DBSDAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
